# BookShelf app

[![Greenkeeper badge](https://badges.greenkeeper.io/kompanycoder/bookShelf.svg)](https://greenkeeper.io/)

# React full-stack Web App

  ```
  About: A bookshelf app where users read books and leave reviews after reading the books.

  Tools:
  - Built using React, Redux, JsonWebToken, Bcrypt, Cookie-parser, Material-ui, Bootstrap, Node, Express and MongoDb.
  
  ```
