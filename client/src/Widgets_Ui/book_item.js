import React from 'react';
import { Link } from 'react-router-dom';

const Component = (item) => {
    return(
        <Link to={`/get_books/${item._id}`} className="HomeItem">
            <div className="Home_Header">
            <h2>{item.title}</h2>
            </div>
            
            <div className="book_bubles_container">
                <div className="review">
                    <h2>{item.review}</h2>
                </div>
                <div className="book_bubble book_author">
                    <span>Author: </span>{item.author}
                </div>
                <div className="book_bubble price">
                    <span><strong>Price: </strong></span>${item.price}
                </div>
                <div className="book_bubble pages">
                    <span>Pages: </span>{item.pages}
                </div>
                <div className="book_bubble rating">
                    <span>Rating: </span>{item.rating}
                </div>
            </div>
        </Link>
    )
}

export default Component