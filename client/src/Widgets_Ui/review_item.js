import React from "react";
import { Link } from "react-router-dom";
// import moment from 'moment';
//to do implement use of moment.js library
const postItem = (item) => {
  
  return (
    <Link
      to={`/user/reviews/${item._id}`} className="HomeItem" key={item._id}>
        <h2>Book Title: {item.title}</h2>
        <h4>Review: {item.review}</h4>
        <h6>Book Author: {item.author}</h6>
        <h6>Last update time_log: {item.updatedAt}</h6>
        <h6>Book review: {item.review}</h6>
     
    </Link>
  );
};

export default postItem;
