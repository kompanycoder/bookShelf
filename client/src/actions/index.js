import axios from "axios";

export function getBooks(skip = 0, limit = 5, sort = "desc", list = "") {
  const request = axios
    .get(`/api/get_books?skip=${skip}&limit=${limit}&sort=${sort}`)
    .then(response => {
      if (list) {
        return [...list, ...response.data];
      } else {
        return response.data;
      }
    });
  return {
    type: "Get_Books",
    payload: request
  };
}

export function getBookWithReviewer(id) {
  const request = axios.get(`/api/get_single_book?id=${id}`);

  return dispatch => {
    request.then(({ data }) => {
      let book = data;

      axios.get(`/api/user_detail?id=${book.owner_Id}`).then(({ data }) => {
        let response = {
          book,
          reviewer: data
        };
        dispatch({
          type: "Get_B_W_Reviewer",
          payload: response
        });
      });
    });
  };
}

export function removeBookWithReviewer() {
  return {
    type: "remove_B_W_Reviewer",
    payload: null
  };
}

// User redux functions
export function loginUser({ email, password }) {
  const request = axios
    .post(`/api/login_user`, { email, password })
    .then(response => response.data);
  return {
    type: "get_User",
    payload: request
  };
}

// is auth function
export function Auth() {
  const request = axios.get(`/api/auth`).then(response => response.data);
  return {
    type: "user_Auth",
    payload: request
  };
}

//register user function
export function registerUser(user) {
  const request = axios
    .post(`/api/register_user`, user)
    .then(response => response.data);
  return {
    type: "Register_User",
    payload: request
  };
}
// get user reviews
export function getUserReviews(id) {
  const request = axios
    .get(`/api/user_posts?id=${id}`)
    .then(response => response.data);
  return {
    type: "Get_User_Reviews",
    payload: request
  };
}
// logout user and reset token
export function logoutUser() {
  const request = axios.get(`/api/logout_user`).then(response => response.data);

  return {
    type: "Logout_User",
    payload: request
  };
}
// edit user reviews
// get user review
// then use state to mount props
// then add delete button and edit and save
export function editUserPost(id) {
  const request = axios.get(`/api/get_single_book?id=${id}`);
  return dispatch => {
    request.then(({ data }) => {
      let post = data;
      axios.get(`/api/user_detail?id=${post.owner_Id}`).then(({ data }) => {
        let reviewer = data;
        let response = {
          reviewer,
          post
        };
        dispatch({
          type: "Edit_User_Post",
          payload: response
        });
      });
    });
  };
}

// post function
export function submitPost(submitReview) {
  // book without owner_id
  let incomplete_book = submitReview;
  console.log(incomplete_book);
  // make auth req n get id
  const request = axios.get(`/api/auth`)
  return (dispatch) => {
    request.then(({data})=>{
      let add_owner_id = {
        owner_Id : data.id
      }
      let complete_book = Object.assign({},incomplete_book,add_owner_id)
      console.log(complete_book);
      axios.post(`/api/post_book`, complete_book).then(response =>{
        let new_book_response = response.data;
        dispatch({
          type: "Post_Review",
          payload: new_book_response
        })
      })
    })   
  }
}

// update function
export function updatePost(postSubmit) {
  const request = axios.post(`/api/update_book`, postSubmit).then(response => response.data);
  console.log(request)
  return {
    type: "Update_Post",
    payload: request
  }
}

// delete function
export function deletePost(id){
  const request = axios.delete(`/api/Delete_book?id=${id}`).then(response => response.data);
  return{
    type: "Delete_Post",
    payload:request
  }
}