import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { Auth } from "../../actions";

export default function(ComposedClass, reload) {
  class Authenticate extends PureComponent {
    state = {
      loading: true
    };
    componentWillMount() {
      this.props.dispatch(Auth());
    }
    componentWillReceiveProps(nextprops) {
      this.setState({ loading: false });
      if (!nextprops.user.login.isAuth) {
        if (reload === true) {
          this.props.history.push("/login");
        }
      } else {
        if (reload === false) {
          this.props.history.push("/user");
        }
      }
    }

    render() {
      console.log(this.props);
      if (this.state.loading === false) {
        return <ComposedClass {...this.props} user={this.props.user} />;
      } else {
        return (
          <div className="loader_div">
            <div className="loader"></div>
            
          </div>
        );
      }
    }
  }
  function mapStateToProps(state) {
    return {
      user: state.user,
      books: state.books
    };
  }

  return connect(mapStateToProps)(Authenticate);
}
