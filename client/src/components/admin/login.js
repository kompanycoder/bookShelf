import React, { Component } from "react";
import { connect } from 'react-redux';
import { loginUser } from '../../actions';

class Login extends Component {
  state = {
    email: '',
    password: '',
    message:''
  };
  handleEmailInput = (event) => {
      this.setState({
          email:event.target.value
      })
    }
  handlePasswordInput = (event) => {
        this.setState({
            password:event.target.value
        })
    }
  submitForm = (e) => {
     e.preventDefault();
     this.props.dispatch(loginUser(this.state))

  };
  
  render() {
      console.log(this.props)
    return (
        
      <div className="form">
        <h1> Login to continue</h1>

        <form onSubmit={this.submitForm} >
            

          <div className="form_div">
            <input
              className="form_element"
              placeholder="Enter your Email"
              value={this.state.email}
              type="email"
              onChange={this.handleEmailInput}
            />
          </div>

          <div className="form_div">
            <input
              className="form_element"
              placeholder="Enter your Password"
              value={this.state.password}
              type="password"
              onChange={this.handlePasswordInput}
            />
          </div>

          <div className="form_div">
            <button type="submit" className="button form_element">Login </button>
          </div>
            {
                this.props.user.login ? 
                <div className="error_msg">
                    {this.props.user.login.message}
                </div>
                :null
            }
        </form>
      </div>
    );
  }
}

function mapStateToProps(state){
   return{
       user:state.user
    }
}
export default connect(mapStateToProps)(Login);
