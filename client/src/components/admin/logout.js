import React, { Component } from 'react';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions';
class Logout extends Component {
  
  componentWillMount () {
    this.props.dispatch(logoutUser())
    
  }
  componentWillReceiveProps(nextprops){
    setTimeout(() => {
      if(nextprops){
        this.props.history.push(`/`)
      }
    }, 4000);
    
  }
  ShowLogoutMessage = () => (
      <div className="book_bubles_container">
        See you next time, You are successfully logged out....
      </div>
  )
    
  render() {
    return (
      <div>
        {this.ShowLogoutMessage()}
      </div>
    )
  }
}
function mapStateToProps(state){
    return {
        user: state.user
    }
}

export default connect(mapStateToProps)(Logout);