import React, { Component } from 'react';
import { connect } from 'react-redux';
import { registerUser } from '../../actions';

class Register extends Component {
  state={
    formdata:{
      email:'',
      password:'',
      firstName:'',
      lastName:''
    }
  }
  handleInput = (event,name) => {
    const newformdata = {
      ...this.state.formdata
    }
    newformdata[name] = event.target.value

    this.setState({
      formdata : newformdata 
    })
  }

  componentWillReceiveProps(nextprops){
    if (nextprops){
      this.props.history.push(`/login`);
    }
  }

  handleSubmit =(e) => {
    e.preventDefault();
    let user = this.state.formdata;
    this.props.dispatch(registerUser(user))
  }
  render() {
    return (
      <div className="form">
       <form onSubmit={this.handleSubmit}>

       <div className="form_div">
            <input
              className="form_element"
              placeholder="Enter your Email"
              value={this.state.formdata.email}
              type="email"
              onChange={(event) => this.handleInput(event,'email')}
            />
        </div>
        <div className="form_div">
        <input
              className="form_element"
              placeholder="Enter your Password"
              value={this.state.formdata.password}
              type="password"
              onChange={(event) => this.handleInput(event,'password')}
            />
        </div>
        <div className="form_div">
        <input
              className="form_element"
              placeholder="Enter your firstname"
              value={this.state.formdata.firstName}
              type="text"
              onChange={(event) => this.handleInput(event,'firstName')}
            />
        </div>
        <div className="form_div">
          <input
                className="form_element"
                placeholder="Enter your lastname"
                value={this.state.formdata.lastName}
                type="text"
                onChange={(event) => this.handleInput(event,'lastName')}
            />
        </div>
        <div className="form_div">
          <button type="submit" className="button form_element">Register </button>
        </div>
       </form>
        
      </div>
    )
  }
}

function mapStateToProps(state){
  return {
    user:state.user
  }
}
export default connect(mapStateToProps)(Register);