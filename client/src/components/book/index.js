import React, { Component} from 'react';
import { connect} from 'react-redux';
import { getBookWithReviewer, removeBookWithReviewer } from '../../actions';

class BookView extends Component {
    componentDidMount(){
        this.props.dispatch(getBookWithReviewer(this.props.match.params.id))
    }
    componentWillUnmount(){
        this.props.dispatch(removeBookWithReviewer(this.props.books))
    }

renderBook = (books) => (
    books.book ? 
        <div className="book_container">
            <div className="book_header"> <h2>{books.book.title}</h2>
            <div className="reviewer_div"><span>Reviewed by: </span> {books.reviewer.firstname} {books.reviewer.lastname}</div>
            </div> 
            <div className="book_content"> 
                <div className="book_header"> {books.book.review} </div>
                <div className="reviewer_div">
                    <div><span>Price: </span> ${books.book.price}</div>
                    <div><span>Rating: </span>{books.book.rating}/5</div>
                </div>
            
            </div> 
        </div>
    : null
)

render(){
    let books =this.props.books
    return(
        <div>
            {this.renderBook(books)}
        </div>
    )
}
}

function mapDispatchToProps (state)  {
    return {
        books:state.books,
        user:state.user
    }
}
export default connect(mapDispatchToProps)(BookView);