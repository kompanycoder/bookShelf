import React, { Component } from "react";
import { connect } from "react-redux";
import { submitPost } from "../../actions";
class addReviews extends Component {
  constructor(props) {
    super(props);

    this.state = {
      review: {
        title: "",
        author: "",
        pages: "",
        price: "",
        rating: "",
        review: ""
      }
    };
  }

  componentWillReceiveProps(nextprops) {
    if (nextprops) {
      this.props.history.push(`/`);
    }
  }
  SubmitNewReview = (e) => {
    e.preventDefault();
    let submitReview = this.state.review;
    this.props.dispatch(submitPost(submitReview));
  };
  handleStateChange = (e, name) => {
    let newReview = { ...this.state.review };
    newReview[name] = e.target.value;

    this.setState({
      review: newReview
    });
  };

  render() {
    console.log(this.state.review);
    return (
      <div className="form">
        {/* Add Review to database using the this.state.but first intergrate it... */}
        <h3>Add new Book review</h3>

        <form onSubmit={this.SubmitNewReview}>
          <div className="form_div">
            <label htmlFor="title">Title for Book : </label> <br />
            <input
              id="title"
              className="form_element"
              onChange={e => this.handleStateChange(e, "title")}
              value={this.state.review.title}
            />
          </div>
          <div className="form_div">
            <label htmlFor="author">Author for Book : </label> <br />
            <input
              id="author"
              className="form_element"
              onChange={e => this.handleStateChange(e, "author")}
              value={this.state.review.author}
            />
          </div>
          <div className="form_div">
            <label htmlFor="page">Pages : </label> <br />
            <input
              id="page"
              className="form_element"
              onChange={e => this.handleStateChange(e, "pages")}
              value={this.state.review.pages}
            />
          </div>
          <div className="form_div">
            <label htmlFor="price">( $ ) Price for Book : </label> <br />
            <input
              id="price"
              className="form_element"
              onChange={e => this.handleStateChange(e, "price")}
              value={this.state.review.price}
            />
          </div>
          <div className="form_div">
            <label htmlFor="rating">(_/5) Rating for Book : </label> <br />
            <input
              id="rating"
              className="form_element"
              onChange={e => this.handleStateChange(e, "rating")}
              value={this.state.review.rating}
            />
          </div>
          <div className="form_div">
            <label htmlFor="comment">comment for Book : </label> <br />
            <input
              id="comment"
              className="form_element"
              onChange={e => this.handleStateChange(e, "review")}
              value={this.state.review.review}
            />
          </div>
          <div className="form_div">
            <button type="submit" className="button form_element">
              {" "}
              Submit
            </button>
          </div>
        </form>
      </div>
    );
  }
}

function MapStateToProps(state) {
  return {
    book: state.book
  };
}

export default connect(MapStateToProps)(addReviews);
