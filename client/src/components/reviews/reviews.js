import React, { Component } from 'react';
import { connect } from 'react-redux';
import { getUserReviews } from "../../actions";
import PostItem from '../../Widgets_Ui/review_item';

class Reviews extends Component { 
  componentWillMount(){
    this.props.dispatch(getUserReviews(this.props.user.login.id))
  }
  
  DisplayPosts = (reviews) => (
    reviews ? 
      reviews.map((item) => <PostItem {...item} key={item._id} />)
    : null
  )
  
  render() {
    
    let userPost = this.props.user.login
    let reviews = this.props.user.reviews
    return (
      <div>
        <h4>Reviews for: {userPost.lastname}</h4>
        <div className="book_container">

        <h5>List of User reviews:</h5><br/><h6 className="edit_click"> Click to edit Users Reviews</h6>
        {this.DisplayPosts(reviews)}</div>
      </div>
    )
  }
}

function mapStateToProps(state){
    return{
      user: state.user,
      review:state.books
    }
}
export default connect(mapStateToProps)(Reviews);