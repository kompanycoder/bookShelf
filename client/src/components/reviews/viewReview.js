import React, { Component } from 'react';
import { connect } from 'react-redux';
import { editUserPost, updatePost, deletePost } from '../../actions';

class EditReview extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      review:{
        _id: '',
        title: '',
        author: '',
        pages: '',
        price: '',
        rating: '',
        review: ''
      }
    }
  }
  
  // play around with mapping state from props and vice versa
  componentWillMount(){
    this.props.dispatch(editUserPost(this.props.match.params.id))
  }
  componentWillReceiveProps(nextProps){
    let mountPost = nextProps.books.post;
    this.setState({
      review: {
        _id: mountPost._id,
        title: mountPost.title,
        author: mountPost.author,
        pages: mountPost.pages,
        price: mountPost.price,
        rating: mountPost.rating,
        review: mountPost.review
      }
    })
  }

  handleStateChange =(e, name)=>{
    let newState = {...this.state.review};
    newState[name] = e.target.value;
    // check on it later
    this.setState({
      review: newState
    })
  }
  deleteGivenPost=()=>{
    let id = this.props.books.post._id;
    this.props.dispatch(deletePost(id));
    this.props.history.push(`/user/reviews`);
  }
  UpdateReview =(e)=>{
    e.preventDefault();
    let postSubmit = this.state.review;
    this.props.dispatch(updatePost(postSubmit));
    // to work on this aftr pushing to git...epic save button
    // create redux edit function
    // send new update and post the new book as Proof
  }
  render() {
    // console.log(this.state.review)
    // let Post = this.props.books.post;
    // thinking of mapping the item on textbox then implement update button and delete button for full editting feature of this full-stack app
    // console.log(Post) 
    // Edit Review to database using the this.state.but first intergrate it...
    // completed 
    return (
      <div>
        <h3>Edit review</h3>
        <div className="form">
          
            <form onSubmit={this.UpdateReview}>
              <div className="form_div">
                <label htmlFor="title">Title for Book : </label> <br/>
                <input id="title" className="form_element" onChange={(e)=>this.handleStateChange(e, 'title')} value={this.state.review.title}/>
              </div>
              <div className="form_div">
                <label htmlFor="author">Author for Book : </label> <br/>
                <input  id= "author" className="form_element" onChange={(e)=>this.handleStateChange(e, 'author')} value={this.state.review.author}/>
              </div>
              <div className="form_div">
                <label htmlFor="page">Pages for book : </label> <br/>
              <input id="page" className="form_element" onChange={(e)=>this.handleStateChange(e, 'pages')} value={this.state.review.pages}/>
              </div>
              <div className="form_div">
                <label htmlFor="price">( $ ) Price for Book : </label> <br/>
              <input id="price" className="form_element" onChange={(e)=>this.handleStateChange(e, 'price')} value={this.state.review.price}/>
              </div>
              <div className="form_div">
                <label htmlFor="rating">(_/5) Rating for Book  : </label> <br/>
                <input id="rating" className="form_element" onChange={(e)=>this.handleStateChange(e,'rating')} value={this.state.review.rating}/>
              </div>
              <div className="form_div">
                <label htmlFor="comment">comment for Book : </label> <br/>
                <input id="comment" className="form_element" onChange={(e)=>this.handleStateChange(e, 'review')} value={this.state.review.review}/>
              </div>
              <div className="form_div">
                <button type="submit" className="button form_element" >Update</button>
                
              </div>
            </form>
            <button type="submit" className="button form_element" onClick={this.deleteGivenPost}>Delete</button>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state){
    return {
      user: state.user,
      review:state.review
    }
}
export default connect(mapStateToProps)(EditReview);
