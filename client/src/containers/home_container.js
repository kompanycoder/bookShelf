import React, { Component } from "react";
import { getBooks } from "../actions";
import { connect } from "react-redux";
import BookItem from "../Widgets_Ui/book_item";

class HomeContainer extends Component {
  componentDidMount() {
    this.props.dispatch(getBooks(0, 3, 'desc'));
  }

  
  homeItems = (books) =>(
    books.list
      ? books.list.map(item => <BookItem {...item} key={item._id} />)
      : null);

     loadMore = () => {
        let count = this.props.books.list.length;
        this.props.dispatch(getBooks(count, count, 'desc', this.props.books.list));
      };
    

  render() {
    return (
      <div>
        {this.homeItems(this.props.books)}
        <div className="LoadMore" onClick={()=>this.loadMore()}>
          Load More Books..
        </div>
      </div>
    );
  }
}
function mapStateToProps(state) {
  return {
    books: state.books
  };
}
export default connect(mapStateToProps)(HomeContainer);
