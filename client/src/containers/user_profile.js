import React, { Component } from 'react'

export default class UserProfile extends Component {
  render(props) {
    let user = this.props.user
    return (
      <div {...this.props.user}>
      <h5 className="User_id_tag">Sign_in Id is: {user.login.id}</h5>
        <div className="User_container">
          <img className="img_responsive"  alt=""/>

          <div className="user_details">
            <h6>User Email: {user.login.email}</h6> <br/>
            <h6>Firstname: {user.login.firstname}</h6> <br/>
            <h6>Lastname: {user.login.lastname}</h6> <br/>
          </div>

        </div>
        
      </div>
    )
  }
}
