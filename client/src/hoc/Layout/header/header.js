import React, { Component } from "react";

import { Link } from "react-router-dom";
import Nav from "./sidenav";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

class Header extends Component {
  state = {
    showNav: false
  };
  onHideNav = () => {
    this.setState({
      showNav: false
    });
  };
  render() {
    return (
      <header className="header">
        <FontAwesomeIcon
          icon="book"
          size="3x"
          className="font_bars"
          onClick={() => {
            this.setState({
              showNav: true
            });
          }}
        />
        <Link to="/" className="header_logo">
          Book Review App
        </Link>

        <Nav showNav={this.state.showNav} onHideNav={() => this.onHideNav()} />
      </header>
    );
  }
}

export default Header;
