import React from 'react'
import SideNav from 'react-simple-sidenav';
import SideNavItems from './sidenavItems';

const Nav = (props) => {
    
    return(
        <SideNav
        showNav={props.showNav}
        onHideNav={props.onHideNav}
        navStyle={{
            background:'#191d1d',
            maxWidth:'200px'
        }}
        >
            <SideNavItems {...props.items}/>
        </SideNav>
    )
}

export default Nav;