import React from 'react';
import {Link} from 'react-router-dom';

const sideNavItems = () => {
  const items = [
    {
      type: "navItem",
      icon: "file-text-o",
      link: "/user",
      text: "My Profile",
      restricted: false
    },
    {
      type: "navItem",
      icon: "file-text-o",
      link: "/user/register",
      text: "Register",
      restricted: false
    },
    {
      type: "navItem",
      icon: "file-text-o",
      link: "/login",
      text: "Login",
      restricted: false
    },
    {
      type: "navItem",
      icon: "file-text-o",
      link: "/user/add",
      text: "Add Review",
      restricted: false
    },
    {
      type: "navItem",
      icon: "file-text-o",
      link: "/user/reviews",
      text: "User Reviews",
      restricted: false
    },
    {
      type: "navItem",
      icon: "file-text-o",
      link: "/user/logout",
      text: "Logout",
      restricted: false
    }
  ];

  const elementItem = (item, i) => (
    <div key={i} className={item.type}>
        <Link to={item.link} className={item.icon} >
            {item.text}
        </Link>
    </div>
    )

  const showNavItems = () =>(
    items.map((item, i) => {
      return elementItem(item, i);
    }))

  return <div>{showNavItems()}</div>;
};

export default sideNavItems;
