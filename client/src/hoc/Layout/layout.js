import React from "react";
import Header from './header/header';
// import Card from '@material-ui/core/Card';

const Layout = (props) => {
  return (
    <div className="layout">
     <Header/>
      <div>{props.children}</div>
    </div>
  );
};

export default Layout;
