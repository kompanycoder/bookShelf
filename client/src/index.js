import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter} from 'react-router-dom';
import { createStore, applyMiddleware} from 'redux';
import promiseMiddleware from 'redux-promise';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';
import Routes from './routes';
import 'bootstrap/dist/css/bootstrap.css';
import './index.css';

import { library } from '@fortawesome/fontawesome-svg-core';
import { faIgloo, faBars, faBaby, faBook, faBookReader } from '@fortawesome/free-solid-svg-icons';
library.add(faIgloo, faBars, faBaby, faBook, faBookReader );

const createStoreWithMiddleware = applyMiddleware(promiseMiddleware,ReduxThunk)(createStore)

ReactDOM.render(
    <Provider store={createStoreWithMiddleware(reducers)}>
        <BrowserRouter>
            <Routes/>
        </BrowserRouter>
    </Provider> ,document.getElementById('root'));
