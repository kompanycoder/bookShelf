export default function (state={}, action) {
    switch(action.type) {
        case 'Get_Books':
            return {...state, list:action.payload};
        case 'Get_B_W_Reviewer':
            return {...state,
                book:action.payload.book,
                reviewer:action.payload.reviewer
            }
        case 'remove_B_W_Reviewer':
            return {...state,
            book:null,
            reviewer:null}
        case 'Edit_User_Post':
            return {...state, post:action.payload.post
            }
        case 'Post_Review':
            return {...state, book: action.payload}
        case 'Update_Post':
            return {...state, post:action.payload}
        case 'Delete_Post':
            return{ ...state, post:action.payload }
        default:
        return state;
    }
}