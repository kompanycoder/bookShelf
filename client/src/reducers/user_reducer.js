export default function (state={}, action) {
    switch(action.type){
        case 'get_User':
            return { ...state, login:action.payload};
        case 'Register_User':
            return { ...state, register:action.payload}; 
        case 'user_Auth':
            return { ...state, login:action.payload};
        case 'Get_User_Reviews':
            return { ...state, reviews:action.payload};
        case 'Logout_User':
            return { ...state, logout:action.payload};
        default:
        return state;
    }
}