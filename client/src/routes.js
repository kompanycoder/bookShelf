import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from "./components/home/home";
import Layout from './hoc/Layout/layout';
import BookView from './components/book';
import Login from './components/admin/login';
import Register from './components/admin/register';
import UserProfile from './containers/user_profile';
import Auth from './components/admin/auth';
import Reviews from './components/reviews/reviews';
import Logout from './components/admin/logout';
import EditReviews from './components/reviews/viewReview';
import AddReviews from './components/reviews/addReviews';

const Routes = () => {
  return (
    <Layout >
      <Switch>
        <Route path="/" exact component={Auth(Home,null)} />
        <Route path="/get_books/:id" exact component={Auth(BookView)} />
        <Route path="/login" exact component={Auth(Login,false)} />
        <Route path="/user/register" exact component={Auth(Register)} />
        <Route path="/user" exact component={Auth(UserProfile,true)} />
        <Route path="/user/reviews" exact component={Auth(Reviews,true)} />
        <Route path="/user/reviews/:id" exact component={Auth(EditReviews,true)} />
        <Route path="/user/add" exact component={Auth(AddReviews,true)} />
        <Route path="/user/logout" exact component={Auth(Logout,true)} />
      </Switch>
    </Layout>
  );
};

export default Routes;
