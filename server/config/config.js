const config = {
  // environment Variables for the app
  production: {
    // production environmnt
    SECRET: "TOPSECRETPASSWORD123456",
    DATABASE: process.env.MONGODB_URI || 'mongoAtlasUrl'
  },
  default: {
    // locolhost environment
    SECRET: "TOPSECRETPASSWORD123456",
    DATABASE: "mongodb://localhost:27017/booksDb"
  }
};

// update environ to es7
exports.get = function get(env) {
  return config[env] || config.default;
};
