const mongoose = require("mongoose");

const BookModel = mongoose.Schema(
  {
    title: {
      type: String,
      required: true
    },
    author: {
      type: String,
      required: true
    },
    type:{
      tyoe: String
    },
    ISBN: {
      type: Number
    },
    pages: {
      type: Number
    },
    genre:{
      type: String
    },
    publisher:{
      type: String
    },
    reviews:[
      {
        type: String
      }
    ],
    prices:[
      {
        b_price: Number,
        b_type: String
      }
    ],
    rating: {
      type: Number,
      min: 1,
      max: 5
    },
    owner_Id: {
      type: String
    }
  },
  {
    timestamps: true
  }
);

const Book = mongoose.model("Book", BookModel);

module.exports = {
  Book
};

// new book model to try out & implement; -done

// {
//   "_id": "book_best_of_times",
//   "_rev": "1-ffe4d573caee404da6c1662e32cf429b",
//   "title": "The Best of times",
//   "author": "author_mary_jenkins",
//   "type": "book",
//   "publisher": "Penguin Books",
//   "ISBN": "12947333",
//   "pages": 199,
//   "genre": "Fiction",
//   "description": "blah",
//   "rating": 78,
//   "prices": [
//     {
//       "type": "paperback",
//       "price": 9.99
//     },
//     {
//       "type": "audiobook",
//       "price": 19.99
//     }
//   ]
// }
