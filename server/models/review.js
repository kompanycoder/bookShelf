// require mongoose
const mongoose = require("mongoose");

// make schema
const ReviewSchema = mongoose.Schema(
  {
    review_owner: {
      type: String,
      required: true
    },
    message: {
      type: String,
      required: true
    },
    replies: [{ type: String }]
  },
  { timestamps: true }
);

// pre action methods
ReviewSchema.pre("save", function(next) {
  let review = this;
  review.save((err, newReview) => {
    if (err) return next(err);
    next(null, newReview);
  });
});

// make model
const Review = mongoose.model("Review", ReviewSchema);
// update requirement of eslint config  for this workspace

module.exports = { Review };
