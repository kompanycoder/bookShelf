const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const config = require("./../config/config").get(process.env.NODE_ENV);

const SALT_I = 10;

const UserSchema = mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true,
      minLen: 6
    },
    firstname: {
      type: String,
      required: true,
      max: 100
    },
    lastname: {
      type: String,
      required: true,
      max: 100
    },
    placeOfBirth: { type: String },
    birthDate: { type: Date },
    reviews: {
      type: Array
    },
    type: { type: String },
    status: {
      type: Number
    }
  },
  { timestamps: true }
);

// check into schema with new es6 moluse functions for callbacks

UserSchema.pre("save", function(next) {
  let user = this;

  if (user.isModified("password")) {
    bcrypt.genSalt(SALT_I, (err, salt) => {
      if (err) throw err;

      // user pass + salt => hash
      bcrypt.hash(user.password, salt, (err, hash) => {
        if(err) throw err;
        user.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});

UserSchema.methods.comparePassword = function (userPassword, next) {
 let user = this;
  bcrypt.compare(userPassword, user.password, (err, isMatch)=> {
    if (err) return next(err);
    next(null, isMatch);
  });
};



// trying to see u=if capital User usage is better than user usage

//generate user token
UserSchema.methods.generateToken = function(next) {
  // logged in user
  let User = this;
  // gen token
  let token = jwt.sign(user._id.toHexString(), config.SECRET);

  // save user token
  User.token = token;
  User.save((err, loggedUser)=> {
    if(err) throw err;
    loggedUser
  });
};


//get user token
UserSchema.statics.findByToken = function(token, next) {
  var user = this; 

  jwt.verify(token, config.SECRET, (err, decode)=> {
    user.findById(
      {
        _id: decode,
        token: token
      },
      (err, User)=> {
        if (err) throw err;
        next(null, User);
      }
    );
  });
};
UserSchema.methods.deleteToken = function(token, next) {
  var user = this;
  token = token;

  user.update(
    {
      $unset: {
        token: 1
      }
    },
    (err, User)=> {
      if(err) return next(err);
      next(null, User);
    }
  );
};

const User = mongoose.model("User", UserSchema);

module.exports = {
  User
};
// new user model implemented
