const router = require("express").Router();
const { Book } = require("../models/book");

//BOOKS API REQUESTS
//get single api
router.get("/api/get_single_book", (req, res, next) => {
  let id = req.query.id;

  //code about getting books from the mongodb database
  Book.findById(id, (err, doc) => {
    if (err) return next(err);
    return res.status(200).json(doc);
  });
});

//debug sorting err...Noted and sorted.
// Get many books and limiting and sorting and maybe skipping for the Home route display
router.get("/api/get_books", (req, res, next) => {
  let skip = parseInt(req.query.skip);
  let limit = parseInt(req.query.limit);
  let sort = req.query.order;

  Book.find()
    .skip(skip)
    .limit(limit)
    .sort({
      _id: sort
    })
    .exec((err, docs) => {
      if (err) return next(err);
      return res.json(docs);
    });
});

//post new book api
router.post("/api/post_book", (req, res, next) => {
  let newBook = new Book(req.body);

  newBook.save((err, doc) => {
    if (err) return next(err);
    return res.status(200).json({
      Book_Success: true,
      Book_id: doc._id,
      New_book: doc
    });
  });
});

//update Book api
router.post("/api/update_book", (req, res, next) => {
  Book.findByIdAndUpdate(
    req.body._id,
    req.body,
    {
      new: true
    },
    (err, doc) => {
      if (err) return next(err);
      return res.json({
        update_success: true,
        doc
      });
    }
  );
});

//delete Book api
router.delete("/api/Delete_book", (req, res, next) => {
  let id = req.query.id;
  Book.findByIdAndRemove(id, (err, delBook) => {
    if (err) return next(err);
    return res.json({
      Deleted_book: true,
      deleted_item: delBook
    });
  });
});

//get user posts on books
router.get("/api/user_posts", (req, res, next) => {
  Book.find({
    owner_Id: req.query.id
  }).exec((err, docs) => {
    if (err) return next(err);
    return res.json(docs);
  });
});

module.exports = router;
