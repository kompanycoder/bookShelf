// require router
const router = require("express").Router();
// require review model
const { Review } = require("../models/review");

// route to get all user reviews
router.get("/api/user/reviews", (req, res, next) => {
  Review.find({}, (err, reviews) => {
    if (err) return next(err);
    res.status(200).json(reviews);
  });
});
// adding a review route
router.post("/api/user/add_review", (req, res, next) => {
  let newReview = new Review(req.body);

  newReview.save((err, newReview) => {
    if (err) return next(err);
    res.status(200).json({
      save_status: true,
      new_item: newReview
    });
  });
});

module.exports = router;
