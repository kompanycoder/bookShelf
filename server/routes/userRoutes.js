const router = require("express").Router();
const { User } = require("../models/user");
const { auth } = require("../middleware/auth");

//USER API REQUESTS

//register user
router.post(`/api/register_user`, (req, res, next) => {
  let newUser = new User(req.body);

  newUser.save((err, savedUser) => {
    if (err) return next(err);
    res.status(200).json({
      registrationSuccess: true,
      newUsr: savedUser
    });
  });
});

//login user
router.post('/api/login_user', (req, res, next) => {
  //  find one user
  User.findById(
    {
      "_id": req.query.id
    },
    (err, User) => {
      if (!User)
        return res.json({
          err,
          isAuth: false,
          message: "Error!! User not found"
        });
      //compare password with bcrypt
      User.comparePassword(req.body.password, (err, isMatch) => {
        if (!isMatch)
          return res.json({
            err,
            isAuth: false,
            message: "Wrong Password!!"
          });
        // generate token to enable deserializing user object
        User.generateToken(User.token, (err, User) => {
          if (err) return next(err);
          res.cookie("auth", User.token).json({
            isAuth: true,
            id: User._id,
            email: User.email,
            token: User.token
          });
        });
      });
    }
  );
});

//get user lastname and firstname
router.get(`/api/user_detail`, (req, res, next) => {
  let id = req.query.id;

  User.findById(id, (err, detailUser) => {
    if (err) return next(err);
    res.status(200).json({
      firstname: detailUser.firstName,
      lastname: detailUser.lastName
    });
  });
});

// get list of users

router.get(`/api/admin/users`, (req, res, next) => {
  // make route available for admin only - to implement -working progress for frontend
  User.find({}, (err, users) => {
    if (err) return next(err);
    res.status(200).json(users);
  });
});

//check if user is authenticated
router.get(`/api/auth`, auth, (req, res) => {
  res.json({
    isAuth: true,
    message: "User is logged in!!",
    id: req.user._id,
    email: req.user.email,
    firstname: req.user.firstName,
    lastname: req.user.lastName
  });
});

//check if user is logged in and log him out
router.get(`/api/logout_user`, auth, (req, res, next) => {
  req.user.deleteToken(req.token, (err, user) => {
    if (err) return next(err);
    res.status(200).json({
      logout_successful: true,
      logged_user: user
    });
  });
});

module.exports = router;
