const Express = require("express");
const bodyParser = require("body-parser");
const morgan = require("morgan");
const cookieParser = require("cookie-parser");
const mongoose = require("mongoose");
const cors = require("cors");
const config = require("./config/config").get(process.env.NODE_ENV);
const app = Express();
// to add to client side app for access to the backend
// "proxy": "http://localhost:3001/",

// include router files;
const userRoutes = require("./routes/userRoutes");
const bookRoutes = require("./routes/bookRoutes");
const reviewroutes = require("./routes/reviewRoutes");

const mongoOptions = {
  useCreateIndex: true,
  useNewUrlParser: true
};
// console.log(config);

mongoose.Promise = global.Promise;
mongoose.connect(config.DATABASE, mongoOptions, (err) => {
  if (err) return err;
  console.log("Connected to localhost BooksDb...")
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.use(morgan("dev"));
app.use(cookieParser());

// make app access the router
app.use(userRoutes);
app.use(bookRoutes);
app.use(reviewroutes);


// init port for the app
const port = process.env.PORT || 3001;
// init listening port
app.listen(port, () => {
  console.log(`server is running on Port: ${port}`);
});


// brainstorm what to add to app
// 1. add books from json api -when in net
//  2. add review schema
// 3 user comments on reviews
//  3. new book schema to implement
// 4. new user chema to implement
// 5 change ui layout and implement card layout for book e
